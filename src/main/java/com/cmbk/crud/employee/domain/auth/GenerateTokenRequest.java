package com.cmbk.crud.employee.domain.auth;

/**
 * @author chanaka.k
 *
 */
public class GenerateTokenRequest {

	private String appId;
	
	private String customerId;
	
	private String apiKey;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	
	

}
