package com.cmbk.crud.employee.domain.auth;

/**
 * @author chanaka.k
 *
 */
public class GenerateTokenResponse {
	
	private String jwsKey;

	public String getJwsKey() {
		return jwsKey;
	}

	public void setJwsKey(String jwsKey) {
		this.jwsKey = jwsKey;
	}

}
